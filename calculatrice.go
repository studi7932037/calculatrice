
package main

import (
	"strconv"  // Importing for string to number conversion
	"strings"  // Importing for string manipulation operations

	"fyne.io/fyne/v2"  // Importing the base Fyne package for GUI app development
	"fyne.io/fyne/v2/app"  // Importing to create the application object
	"fyne.io/fyne/v2/container"  // Importing to use containers for widget layout
	"fyne.io/fyne/v2/widget"  // Importing to use widgets like buttons and entry fields
)

func main() {
    myApp := app.New()  // Initialize a new application
    myWindow := myApp.NewWindow("Calculatrice")  // Create a new window titled 'Calculatrice'

    entry := widget.NewEntry()  // Create an entry widget for input
    entry.SetPlaceHolder("0")  // Set placeholder text for the entry widget

    // Define a closure function for calculations
    calculer := func(expression string) string {
        parts := strings.Fields(expression)  // Split the expression into parts
        if len(parts) != 3 {  // Check if expression is not in 'num op num' format
            return "Erreur"  // Return error if not correct format
        }

        // Parse the numbers from the expression
        num1, err1 := strconv.ParseFloat(parts[0], 64)  // Convert first part to float
        num2, err2 := strconv.ParseFloat(parts[2], 64)  // Convert third part to float
        if err1 != nil || err2 != nil {  // Check for errors in conversion
            return "Erreur"  // Return error if conversion fails
        }

        // Perform the operation based on the second part of the expression
        switch parts[1] {
        case "+":
            return strconv.FormatFloat(num1+num2, 'f', -1, 64)  // Addition
        case "-":
            return strconv.FormatFloat(num1-num2, 'f', -1, 64)  // Subtraction
        case "*":
            return strconv.FormatFloat(num1*num2, 'f', -1, 64)  // Multiplication
        case "/":
            if num2 == 0 {
                return "Erreur"  // Return error if division by zero
            }
            return strconv.FormatFloat(num1/num2, 'f', -1, 64)  // Division
        default:
            return "Op Inconnue"  // Return error if operator is unknown
        }
    }

    // Define a closure to append text to the entry field
    addTextToEntry := func(text string) {
        entry.SetText(entry.Text + text)  // Append the text to the current entry text
    }

    // Create buttons with labels and define their click behavior
    btn1 := widget.NewButton("1", func() { addTextToEntry("1") })
    btn2 := widget.NewButton("2", func() { addTextToEntry("2") })
    btn3 := widget.NewButton("3", func() { addTextToEntry("3") })
    btn4 := widget.NewButton("4", func() { addTextToEntry("4") })
    btn5 := widget.NewButton("5", func() { addTextToEntry("5") })
    btn6 := widget.NewButton("6", func() { addTextToEntry("6") })
    btn7 := widget.NewButton("7", func() { addTextToEntry("7") })
    btn8 := widget.NewButton("8", func() { addTextToEntry("8") })
    btn9 := widget.NewButton("9", func() { addTextToEntry("9") })
    btn0 := widget.NewButton("0", func() { addTextToEntry("0") })
    btnPlus := widget.NewButton("+", func() { addTextToEntry(" + ") })
    btnMinus := widget.NewButton("-", func() { addTextToEntry(" - ") })
    btnMultiply := widget.NewButton("*", func() { addTextToEntry(" * ") })
    btnDivide := widget.NewButton("/", func() { addTextToEntry(" / ") })
    // Button for clearing the entry field
    btnClear := widget.NewButton("C", func() { entry.SetText("") })  // Clear button
    // Button for performing the calculation
    btnEgal := widget.NewButton("=", func() {
        resultat := calculer(entry.Text)  // Calculate result from entry text
        entry.SetText(resultat)  // Set entry text to result
    })

    // Organize buttons in a grid with 4 columns
    grid := container.NewGridWithColumns(4,
        btn7, btn8, btn9, btnDivide,  // Row 1
        btn4, btn5, btn6, btnMultiply,  // Row 2
        btn1, btn2, btn3, btnMinus,  // Row 3
        btnClear, btn0, btnEgal, btnPlus,  // Row 4
    )

    // Resize the window to a wider size to fit the grid
    myWindow.Resize(fyne.NewSize(400, 250))

    // Set the window content to a vertical box layout containing the entry and the grid
    myWindow.SetContent(container.NewVBox(
        entry,  // Entry field at the top
        grid,   // Grid of buttons below the entry field
    ))

    // Show and run the application
    myWindow.ShowAndRun()
}
